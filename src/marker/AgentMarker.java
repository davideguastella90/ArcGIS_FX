package marker;

import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;

import agent.base.AgentType;

public abstract class AgentMarker {
	
	
	//private static final int BLUE = 0xFF0000FF;
	//private static final int GREEN = 0xFF00FF00;

	
	protected final static int MARKER_SIZE = 18;
	
	private AgentType agentType;

	private Point point;

	private SimpleMarkerSymbol symbol;

	private Graphic graphic;

	protected AgentMarker(AgentType agentType, Point point) {
		this.agentType = agentType;
		this.point = point;
		
		
	}

	public AgentType getAgentType() {
		return agentType;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public SimpleMarkerSymbol getSymbol() {
		return symbol;
	}

	protected void setSymbol(SimpleMarkerSymbol symbol) {
		this.symbol = symbol;
	}

	public Graphic getGraphic() {
		return graphic;
	}

	protected void setGraphic(Graphic graphic) {
		this.graphic = graphic;
	}

}

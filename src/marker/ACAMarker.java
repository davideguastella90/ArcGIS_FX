package marker;

import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;

import agent.base.AgentType;

public class ACAMarker extends AgentMarker {

	private static final int RED = 0xFFFF0000;
	
	public ACAMarker(Point point) {
		super(AgentType.AMBIENT_CONTEXT_AGENT, point);
		
		this.setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, RED, MARKER_SIZE));
		
		
		setGraphic(new Graphic(point, getSymbol()));
		//graphic.getAttributes().put("NAME", names.get(i));
		//graphic.getAttributes().put("DESCRIPTION", descriptions.get(i));
	}

	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ACA at "+ getPoint().toString();
	}
	
}

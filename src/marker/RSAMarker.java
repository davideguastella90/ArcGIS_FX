package marker;

import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;

import agent.base.AgentType;

public class RSAMarker extends AgentMarker {
	private static final int PURPLE = 0xFF800080;

	public RSAMarker(Point point) {
		super(AgentType.AMBIENT_CONTEXT_AGENT, point);

		setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.TRIANGLE, PURPLE, MARKER_SIZE));
		setGraphic(new Graphic(point, getSymbol()));
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "RSA at " + getPoint().toString();
	}
}

package application;

import java.io.IOException;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class AppLauncher extends Application {

	private MapAppController controller;

	@Override
	public void start(Stage primaryStage) throws IOException {

		/*
		 * NECESSARY to make the app work
		 */
		ArcGISRuntimeEnvironment.setInstallDirectory("C:\\arcgis-runtime-sdk-java-100.8.0");

		try {
			FXMLLoader loader = new FXMLLoader(AppLauncher.this.getClass().getResource("/ui/fxml/mainUI.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root, 1156, 866);
			primaryStage.setScene(scene);
			primaryStage.setTitle("HybridIoT");

			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent t) {
					Platform.exit();
					System.exit(0);
				}
			});

			controller = loader.getController();

			controller.initMap();

			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Stops and releases all resources used in application.
	 */
	@Override
	public void stop() {
		controller.dispose();
	}

	public static void main(String[] args) {
		launch(args);
	}
}

package application;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.tasks.geocode.SuggestResult;

import javafx.application.Platform;
import javafx.scene.control.ComboBox;

/**
 * A listener to update a {@link ComboBox} when suggestions from a call to
 * {@link LocatorTask#suggestAsync(String, SuggestParameters)} are loaded.
 */
public class SuggestionsLoadedListener implements Runnable {

	private final ListenableFuture<List<SuggestResult>> results;
	private final ComboBox<String> comboBox;

	/**
	 * Constructs a listener to update an auto-complete list for geocode
	 * suggestions.
	 *
	 * @param results suggestion results from a {@link LocatorTask}
	 * @param box     the {@link ComboBox} to update with the suggestions
	 */
	SuggestionsLoadedListener(ListenableFuture<List<SuggestResult>> results, ComboBox<String> box) {
		this.results = results;
		this.comboBox = box;
	}

	@Override
	public void run() {

		try {
			List<SuggestResult> suggestResult = results.get();
			List<String> suggestions = suggestResult.stream().map(SuggestResult::getLabel).collect(Collectors.toList());

			// update the combo box with suggestions
			Platform.runLater(() -> {
				comboBox.getItems().clear();
				comboBox.getItems().addAll(suggestions);
				comboBox.show();
			});

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
}
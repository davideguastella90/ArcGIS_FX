package application;

import java.util.ArrayList;
import java.util.List;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.Polyline;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.tasks.geocode.GeocodeParameters;
import com.esri.arcgisruntime.tasks.geocode.GeocodeResult;
import com.esri.arcgisruntime.tasks.geocode.LocatorTask;
import com.esri.arcgisruntime.tasks.geocode.SuggestParameters;
import com.esri.arcgisruntime.tasks.geocode.SuggestResult;

import confidenceZone.geometry.Polygon;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import marker.ACAMarker;
import marker.AgentMarker;
import marker.RSAMarker;

public class MapAppController {

	private static final String RSA_STRING = "Real Sensor Agent";
	private static final String ACA_STRING = "Ambient Context Agent";

	private static final int MIN_SCALE = 250000;
	private static final int MAX_SCALE = 10000;

	// colors for symbols
	private static final int PURPLE = 0xFF800080;
	private static final int BLUE = 0xFF0000FF;
	private static final int RED = 0xFFFF0000;
	private static final int GREEN = 0xFF00FF00;

	private List<SimpleMarkerSymbol> markers;

	private MapView mapView;

	private GraphicsOverlay graphicsOverlay;

	private Point2D mapViewPoint;

	private Graphic identifiedGraphic;

	@FXML
	ComboBox<String> agentComboBox;

	ObservableList<AgentMarker> markersList;

	@FXML
	ListView<AgentMarker> agentList;

	@FXML
	private VBox vbox;

	private List<AgentMarker> myMarkers;

	@FXML
	private ComboBox<String> locationBox;

	@FXML
	private ComboBox<String> placeBox;

	private LocatorTask locatorTask;

	public MapAppController() {
		myMarkers = new ArrayList<>();

		markersList = FXCollections.observableArrayList();

	}

	@FXML
	void initialize() {
		agentComboBox.getItems().add(RSA_STRING);
		agentComboBox.getItems().add(ACA_STRING);

		agentList.setItems(markersList);

		// create a locatorTask task
		locatorTask = new LocatorTask("https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer");

	}

	private void setMapViewListener() {
		mapView.setOnMouseClicked(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.isStillSincePress()) {
				// set the cursor to default
				mapView.setCursor(Cursor.DEFAULT);

				// clear any selected graphic
				graphicsOverlay.clearSelection();

				// create a point where the user clicked
				mapViewPoint = new Point2D(e.getX(), e.getY());

				// identify graphics on the graphics overlay
				ListenableFuture<IdentifyGraphicsOverlayResult> identifyGraphics = mapView
						.identifyGraphicsOverlayAsync(graphicsOverlay, mapViewPoint, 10, false);

				identifyGraphics.addDoneListener(() -> {
					try {
						if (!identifyGraphics.get().getGraphics().isEmpty()) {
							// get the first identified graphic
							identifiedGraphic = identifyGraphics.get().getGraphics().get(0);
							// select the identified graphic
							identifiedGraphic.setSelected(true);

							// enable dragging of the identified graphic to move its location
							mapView.setOnMouseDragged(event -> {
								if (identifiedGraphic.isSelected() && identifiedGraphic != null) {
									// set the cursor to closed hand to indicate graphic dragging is active
									mapView.setCursor(Cursor.CLOSED_HAND);
									// create a point from the dragged location
									mapViewPoint = new Point2D(event.getX(), event.getY());
									Point mapPoint = mapView.screenToLocation(mapViewPoint);
									// update the location of the graphic to the dragged location
									identifiedGraphic.setGeometry(mapPoint);
								}
							});
						}
					} catch (Exception x) {
						new Alert(Alert.AlertType.ERROR, "Error identifying clicked graphic").show();
					}
				});
			}
		});
	}

	/**
	 * Creates four Graphics with a location, a symbol, and two attributes. Then
	 * adds those Graphics to the GraphicsOverlay.
	 */
	private void createGraphics() {

		Graphic graphic;
		// create spatial reference for the points
		SpatialReference spatialReference = SpatialReferences.getWebMercator();

		// create points to place markers
		List<Point> points = new ArrayList<>();
		points.add(new Point(-2.641, 56.077, spatialReference));
		points.add(new Point(-2.669, 56.058, spatialReference));
		points.add(new Point(-2.718, 56.060, spatialReference));
		points.add(new Point(-2.720, 56.073, spatialReference));

		// create simple marker symbols for the points
		markers = new ArrayList<>();
		markers.add(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, RED, 10));
		markers.add(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.TRIANGLE, PURPLE, 10));
		markers.add(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CROSS, GREEN, 10));
		markers.add(new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.DIAMOND, BLUE, 10));

		// create a list of names for graphics
		List<String> names = new ArrayList<>();
		names.add("LAMB");
		names.add("CANTY BAY");
		names.add("NORTH BERWICK");
		names.add("FIDRA");

		// create a list of descriptions for graphics
		List<String> descriptions = new ArrayList<>();
		descriptions.add("Just opposite of Bass Rock.");
		descriptions.add("100m long and 50m wide.");
		descriptions.add("Lighthouse in northern section.");
		descriptions.add("Also known as Barley Farmstead.");

		// create four graphics with attributes and add to graphics overlay
		for (int i = 0; i < 4; i++) {
			graphic = new Graphic(points.get(i), markers.get(i));
			graphic.getAttributes().put("NAME", names.get(i));
			graphic.getAttributes().put("DESCRIPTION", descriptions.get(i));
			graphicsOverlay.getGraphics().add(graphic);
		}
	}

	private void initcomboboxes() {
		// event to get auto-complete suggestions when the user types a place query
		placeBox.getEditor().setOnKeyTyped((KeyEvent evt) -> {

			// get the search box text for auto-complete suggestions
			String typed = placeBox.getEditor().getText();

			if (!"".equals(typed)) {

				// suggest places only
				SuggestParameters geocodeParameters = new SuggestParameters();
				geocodeParameters.getCategories().add("POI");

				// get suggestions from the locatorTask
				ListenableFuture<List<SuggestResult>> suggestions = locatorTask.suggestAsync(typed, geocodeParameters);

				// add a listener to update suggestions list when loaded
				suggestions.addDoneListener(new SuggestionsLoadedListener(suggestions, placeBox));
			}
		});

		// event to get auto-complete suggestions for location when the user types a
		// search location
		locationBox.getEditor().setOnKeyTyped((KeyEvent evt) -> {

			// get the search box text for auto-complete suggestions
			String typed = locationBox.getEditor().getText();

			if (!typed.equals("")) {

				// get suggestions from the locatorTask
				ListenableFuture<List<SuggestResult>> suggestions = locatorTask.suggestAsync(typed);

				// add a listener to update suggestions list when loaded
				suggestions.addDoneListener(new SuggestionsLoadedListener(suggestions, locationBox));
			}
		});
	}

	public void initMap() {

		// create a MapView to display the map and add it to the stack pane
		// MapView mapView = new MapView();
		mapView = new MapView();

		// create a graphics overlay
		GraphicsOverlay graphicsOverlay = new GraphicsOverlay();

		// add graphics overlay to the map view
		mapView.getGraphicsOverlays().add(graphicsOverlay);

		vbox.getChildren().add(mapView);

		// create an ArcGISMap with the default imagery basemap
		ArcGISMap map = new ArcGISMap(Basemap.Type.OPEN_STREET_MAP, 43.575075, 1.452858, 16);

		map.setMinScale(MIN_SCALE);
		map.setMaxScale(MAX_SCALE);

		mapView.setMap(map);

		vbox.heightProperty().addListener((obs, oldVal, newVal) -> mapView.setPrefHeight((double) newVal));
		vbox.widthProperty().addListener((obs, oldVal, newVal) -> mapView.setPrefWidth((double) newVal));

		mapView.setOnMouseClicked(e -> {

			// if (e.getButton() == MouseButton.PRIMARY && !e.isStillSincePress()) {
			mapViewPoint = new Point2D(e.getX(), e.getY());
			Point mapPoint = mapView.screenToLocation(mapViewPoint);

			if (agentComboBox.getSelectionModel().getSelectedItem().equals(ACA_STRING)) {
				ACAMarker marker = new ACAMarker(mapPoint);
				myMarkers.add(marker);

				createConfidenceZone(graphicsOverlay, mapView, mapPoint);
				graphicsOverlay.getGraphics().add(marker.getGraphic());

				markersList.add(marker);
			} else if (agentComboBox.getSelectionModel().getSelectedItem().equals(RSA_STRING)) {
				RSAMarker marker = new RSAMarker(mapPoint);
				myMarkers.add(marker);

				graphicsOverlay.getGraphics().add(marker.getGraphic());

				markersList.add(marker);
			} else {

			}

			System.err.println("Agents: #" + Integer.toString(markersList.size()));
			// }

		});

		initcomboboxes();
	}

	private int GetZoomLevel(MapView mapView) {
		double currentRes = mapView.getMapScale();

		return 0;
	}

	private void createConfidenceZone(GraphicsOverlay graphicsOverlay, MapView mapView, Point mapPoint) {
		Point2D center = mapView.locationToScreen(mapPoint);

		SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFF800080, 4);

		Polygon p = Polygon.getRegularPolygon(center.getX(), center.getY(), 10, 8, 100);

		PointCollection points = new PointCollection(SpatialReferences.getWebMercator());

		for (int i = 0; i < p.getNpoints(); i++) {

			Point2D pd = new Point2D(p.getXPoints()[i], p.getYPoints()[i]);
			Point pp = mapView.screenToLocation(pd);
			points.add(pp);
		}

		// line from the last point to the first point
		Point2D pd = new Point2D(p.getXPoints()[0], p.getYPoints()[0]);
		Point pp = mapView.screenToLocation(pd);
		points.add(pp);

		// create the polyline from the point collection
		Polyline polyline = new Polyline(points);

		// create the graphic with polyline and symbol
		Graphic graphic = new Graphic(polyline, lineSymbol);

		double currentRes = mapView.getMapScale();

		System.err.println("added CZ, scale: " + Double.toString(currentRes));

		// add graphic to the graphics overlay
		graphicsOverlay.getGraphics().add(graphic);
	}

	@FXML
	public void search() {
		String placeQuery = placeBox.getEditor().getText();
		String locationQuery = locationBox.getEditor().getText();
		if (/* placeQuery != null && */locationQuery != null
				&& /* !"".equals(placeQuery) && */ !"".equals(locationQuery)) {
			GeocodeParameters geocodeParameters = new GeocodeParameters();
			geocodeParameters.getResultAttributeNames().add("*"); // return all attributes
			geocodeParameters.setOutputSpatialReference(mapView.getSpatialReference());

			// run the locatorTask geocode task
			ListenableFuture<List<GeocodeResult>> results = locatorTask.geocodeAsync(locationQuery, geocodeParameters);
			results.addDoneListener(() -> {
				try {
					List<GeocodeResult> points = results.get();
					if (points.size() > 0) {
						// create a search area envelope around the location
						Point p = points.get(0).getDisplayLocation();
						Envelope preferredSearchArea = new Envelope(p.getX() - 10000, p.getY() - 10000,
								p.getX() + 10000, p.getY() + 10000, p.getSpatialReference());
						// set the geocode parameters search area to the envelope
						geocodeParameters.setSearchArea(preferredSearchArea);
						// zoom to the envelope
						mapView.setViewpointAsync(new Viewpoint(preferredSearchArea));
						// perform the geocode operation
						/*
						 * ListenableFuture<List<GeocodeResult>> geocodeTask =
						 * locatorTask.geocodeAsync(placeQuery, geocodeParameters);
						 */
						// add a listener to display the results when loaded
						// geocodeTask.addDoneListener(new ResultsLoadedListener(geocodeTask));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}

	}

	public void dispose() {
		if (mapView != null) {
			mapView.dispose();
		}
	}

}
